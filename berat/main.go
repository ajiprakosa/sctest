package main

import (
	"database/sql"
	"fmt"
	"html/template"
	"log"
	"net/http"
	"strconv"
	"strings"
	"time"

	"github.com/gorilla/mux"
	_ "github.com/mattn/go-sqlite3"
)

//DBFiles : lokasi database
const DBFiles string = "./data/berat.db"

//MetaPage struct : mendefinisikan object struktur untuk sebuah halaman website
type MetaPage struct {
	HeadTitle string
	Data      interface{}
}

//BeratBadan struct : mendefinisikan object struktur BeratBadan
type BeratBadan struct {
	ID       int
	Tanggal  string
	BeratMax float64
	BeratMin float64
}

//HomePageHandler : index page
func HomePageHandler(w http.ResponseWriter, r *http.Request) {
	database, err := sql.Open("sqlite3", DBFiles)

	if err != nil {
		log.Fatal(err)
	}

	//Row Berat Badan
	var data = BeratBadan{}
	var recordData = []BeratBadan{}
	rows, _ := database.Query("SELECT * FROM data_berat ORDER BY tanggal DESC")
	for rows.Next() {
		_ = rows.Scan(&data.ID, &data.Tanggal, &data.BeratMax, &data.BeratMin)
		recordData = append(recordData, data)
	}
	rows.Close()

	//AVERAGE BERAT MAX
	var avgMax float64

	rowMax, _ := database.Query("SELECT AVG(berat_max) FROM data_berat")
	for rowMax.Next() {
		_ = rowMax.Scan(&avgMax)
	}
	rowMax.Close()

	//AVERAGE BERAT MIN
	var avgMin float64

	rowMin, _ := database.Query("SELECT AVG(berat_min) FROM data_berat")
	for rowMin.Next() {
		_ = rowMin.Scan(&avgMin)
	}
	rowMin.Close()

	//AVERAGE SELISIH
	var avgSelisih float64

	rowSelisih, _ := database.Query("SELECT AVG(berat_max - berat_min) FROM data_berat")
	for rowSelisih.Next() {
		_ = rowSelisih.Scan(&avgSelisih)
	}
	rowSelisih.Close()

	database.Close()

	dataPage := map[string]interface{}{}
	dataPage["average_max"] = strings.TrimRight(strings.TrimRight(fmt.Sprintf("%.2f", avgMax), "0"), ".")
	dataPage["average_min"] = strings.TrimRight(strings.TrimRight(fmt.Sprintf("%.2f", avgMin), "0"), ".")
	dataPage["average_selisih"] = strings.TrimRight(strings.TrimRight(fmt.Sprintf("%.2f", avgSelisih), "0"), ".")
	dataPage["berat_items"] = recordData

	tp := HelperGetTemplatePart("html/list_berat.html")
	mP := MetaPage{HeadTitle: "Informasi Berat Badan", Data: dataPage}
	tp.ExecuteTemplate(w, "layout", mP)
}

//BeratCreatePageHandler : create informasi berat
func BeratCreatePageHandler(w http.ResponseWriter, r *http.Request) {

	dataPage := map[string]map[string]string{}

	if r.Method == "POST" {
		if errParseForm := r.ParseForm(); errParseForm != nil {
			fmt.Fprintf(w, "ParseForm() err: %v", errParseForm)
			return
		}

		tanggal := r.FormValue("tanggal")
		beratMax, _ := strconv.ParseFloat(r.FormValue("berat_max"), 32)
		beratMin, _ := strconv.ParseFloat(r.FormValue("berat_min"), 32)

		dataPage = ValidationBeratBadan(tanggal, beratMax, beratMin)
		//if no error
		if len(dataPage["error"]) == 0 {
			//format tanggal to unix timestamp
			timeFormat, _ := time.Parse(time.RFC3339, tanggal+"T00:00:59+00:00")
			timeStamp := timeFormat.Unix()
			//log.Println(timeStamp)
			//insert ke database
			database, err := sql.Open("sqlite3", DBFiles)
			if err != nil {
				log.Fatal(err)
			}

			sqlQueryStatement := "INSERT INTO data_berat(tanggal, berat_max, berat_min) values(?,?,?);"
			sqlQuery, errPrepare := database.Prepare(sqlQueryStatement)

			if errPrepare != nil {
				log.Println(errPrepare.Error())
			}

			_, errorExec := sqlQuery.Exec(timeStamp, beratMax, beratMin)

			database.Close()

			if errorExec != nil {
				dataPage["error"]["data_insert"] = "Data gagal disimpan."
			} else {
				http.Redirect(w, r, "/", http.StatusSeeOther)
			}

		}

	}

	dataPage["additional_info"] = make(map[string]string)
	dataPage["additional_info"]["page_title"] = "Buat Data Baru"

	tp := HelperGetTemplatePart("html/form_berat.html")
	mP := MetaPage{HeadTitle: "Buat Data Baru", Data: dataPage}
	tp.ExecuteTemplate(w, "layout", mP)
}

//BeratViewPageHandler : view detail informasi berat
func BeratViewPageHandler(w http.ResponseWriter, r *http.Request) {
	//extract variable/parameter
	vars := mux.Vars(r)
	id, _ := strconv.Atoi(vars["id"])

	dataBerat := HelperGetDataBeratByID(id)

	tp := HelperGetTemplatePart("html/detail_berat.html")
	mP := MetaPage{HeadTitle: "Informasi Detail Berat Badan", Data: dataBerat}
	tp.ExecuteTemplate(w, "layout", mP)
}

//BeratEditPageHandler : edit informasi berat
func BeratEditPageHandler(w http.ResponseWriter, r *http.Request) {

	dataPage := map[string]map[string]string{}

	//extract variable/parameter
	vars := mux.Vars(r)
	id, _ := strconv.Atoi(vars["id"])

	dataBerat := HelperGetDataBeratByID(id)

	i, _ := strconv.ParseInt(dataBerat.Tanggal, 10, 64)
	tm := time.Unix(i, 0)

	dataPage = ValidationBeratBadan(tm.Format("2006-01-02"), dataBerat.BeratMax, dataBerat.BeratMin)

	if r.Method == "POST" {
		if errParseForm := r.ParseForm(); errParseForm != nil {
			fmt.Fprintf(w, "ParseForm() err: %v", errParseForm)
			return
		}

		tanggal := r.FormValue("tanggal")
		beratMax, _ := strconv.ParseFloat(r.FormValue("berat_max"), 32)
		beratMin, _ := strconv.ParseFloat(r.FormValue("berat_min"), 32)

		dataPage = ValidationBeratBadan(tanggal, beratMax, beratMin)
		//if no error
		if len(dataPage["error"]) == 0 {
			//format tanggal to unix timestamp
			timeFormat, _ := time.Parse(time.RFC3339, tanggal+"T00:00:59+00:00")
			timeStamp := timeFormat.Unix()
			//log.Println(timeStamp)
			//insert ke database
			database, err := sql.Open("sqlite3", DBFiles)

			if err != nil {
				log.Fatal(err)
			}

			sqlQueryStatement := "UPDATE data_berat SET tanggal = $1, berat_max = $2, berat_min = $3 WHERE id = $4;"
			_, errorExec := database.Exec(sqlQueryStatement, timeStamp, beratMax, beratMin, vars["id"])

			database.Close()

			if errorExec != nil {
				dataPage["error"]["data_insert"] = "Data gagal disimpan."
			} else {
				http.Redirect(w, r, "/", http.StatusSeeOther)
			}

		}

	}

	dataPage["additional_info"] = make(map[string]string)
	dataPage["additional_info"]["page_title"] = "Edit Data"

	tp := HelperGetTemplatePart("html/form_berat.html")
	mP := MetaPage{HeadTitle: "Edit Detail Berat Badan", Data: dataPage}
	tp.ExecuteTemplate(w, "layout", mP)
}

//BeratDeleteHandler : edit informasi berat
func BeratDeleteHandler(w http.ResponseWriter, r *http.Request) {
	//extract variable/parameter
	vars := mux.Vars(r)

	database, err := sql.Open("sqlite3", DBFiles)
	if err != nil {
		log.Fatal(err)
	}
	_, errDelete := database.Exec("DELETE FROM data_berat WHERE id=$1", vars["id"])
	if errDelete != nil {
		log.Fatal(errDelete)
	}

	database.Close()

	http.Redirect(w, r, "/", http.StatusSeeOther)
}

// ValidationBeratBadan : mevalidasi form input
func ValidationBeratBadan(tanggal string, beratMax float64, beratMin float64) map[string]map[string]string {
	dataMessage := map[string]map[string]string{}

	dataMessage["error"] = make(map[string]string)
	dataMessage["currentValue"] = make(map[string]string)

	if tanggal == "" {
		dataMessage["error"]["tanggal"] = "Tanggal tidak boleh kosong."
	} else {
		dataMessage["currentValue"]["tanggal"] = tanggal
	}

	if beratMax == 0 {
		dataMessage["error"]["beratMax"] = "Berat maximum tidak boleh kosong / harus numeric."
	} else {
		dataMessage["currentValue"]["beratMax"] = strings.TrimRight(strings.TrimRight(fmt.Sprintf("%.2f", beratMax), "0"), ".")
	}

	if beratMin == 0 {
		dataMessage["error"]["beratMin"] = "Berat minimum tidak boleh kosong / harus numeric."
	} else {
		dataMessage["currentValue"]["beratMin"] = strings.TrimRight(strings.TrimRight(fmt.Sprintf("%.2f", beratMin), "0"), ".")
	}

	if beratMin > beratMax {
		dataMessage["error"]["beratMinMax"] = "Berat minimum tidak boleh melebihi berat maksimum."
	} else {
		dataMessage["currentValue"]["beratMin"] = strings.TrimRight(strings.TrimRight(fmt.Sprintf("%.2f", beratMin), "0"), ".")
		dataMessage["currentValue"]["beratMax"] = strings.TrimRight(strings.TrimRight(fmt.Sprintf("%.2f", beratMax), "0"), ".")
	}

	return dataMessage
}

//HelperGetDataBeratByID : Retrieve Data Berat by ID
func HelperGetDataBeratByID(id int) BeratBadan {
	database, err := sql.Open("sqlite3", DBFiles)

	if err != nil {
		log.Fatal(err)
	}

	var dataBerat BeratBadan

	sqlStatement := "SELECT * FROM data_berat WHERE id=$1;"

	row := database.QueryRow(sqlStatement, id)
	errScan := row.Scan(&dataBerat.ID, &dataBerat.Tanggal, &dataBerat.BeratMax, &dataBerat.BeratMin)

	if errScan != nil {
		log.Fatal(errScan)
	}

	database.Close()

	return dataBerat
}

//HelperGetTemplatePart : prepare template files
func HelperGetTemplatePart(htmlPart string) *template.Template {
	tp, _ := template.New("").Funcs(template.FuncMap{
		"minus": func(a, b float64) string {
			result := a - b
			return strings.TrimRight(strings.TrimRight(fmt.Sprintf("%.2f", result), "0"), ".")
		},
		"to_string_date": func(timestamp string) string {
			i, _ := strconv.ParseInt(timestamp, 10, 64)
			tm := time.Unix(i, 0)
			return tm.Format("2006-01-02")
		},
		"two_decimal_number": func(number float64) string {
			return strings.TrimRight(strings.TrimRight(fmt.Sprintf("%.2f", number), "0"), ".")
		},
	}).ParseFiles("html/index.html", htmlPart)

	return tp
}
func main() {
	PrepareDatabase()
	routePath := mux.NewRouter()
	routePath.HandleFunc("/", HomePageHandler)
	routePath.HandleFunc("/berat/create", BeratCreatePageHandler)
	routePath.HandleFunc("/berat/{id}", BeratViewPageHandler)
	routePath.HandleFunc("/berat/edit/{id}", BeratEditPageHandler)
	routePath.HandleFunc("/berat/delete/{id}", BeratDeleteHandler)

	//fix path for css, js, image
	routePath.PathPrefix("/css/").Handler(http.StripPrefix("/css/",
		http.FileServer(http.Dir("html/css/"))))

	routePath.PathPrefix("/js/").Handler(http.StripPrefix("/js/",
		http.FileServer(http.Dir("html/js/"))))

	routePath.PathPrefix("/images/").Handler(http.StripPrefix("/images/",
		http.FileServer(http.Dir("html/images/"))))

	http.Handle("/", routePath)
	// Bind route to http listener
	log.Fatal(http.ListenAndServe(":8000", routePath))
}

//PrepareDatabase : Menginisiasi struktur database yang akan dipakai
func PrepareDatabase() {
	database, err := sql.Open("sqlite3", DBFiles)

	if err != nil {
		log.Fatal(err)
	}

	beratDatabaseSchema := "CREATE TABLE IF NOT EXISTS data_berat(" +
		"id INTEGER PRIMARY KEY," +
		"tanggal INTEGER," +
		"berat_max DECIMAL(10,2)," +
		"berat_min DECIMAL(10,2));"

	sqlQueryBeratBadan, errCreateSchemaBerat := database.Prepare(beratDatabaseSchema)

	if errCreateSchemaBerat != nil {
		log.Fatal(errCreateSchemaBerat)
	}

	sqlQueryBeratBadan.Exec()

	database.Close()
}
