<?php
include 'cart.php';
use PHPUnit\Framework\TestCase;

class CartTest extends TestCase{
    public function testTambahProduk()
    {
        $keranjang = new Cart();
        $this->assertEquals(true,$keranjang->tambahProduk('Topi',2));
    }

    public function testTambahProdukTanpaJudul()
    {
        $keranjang = new Cart();
        $this->assertEquals(false,$keranjang->tambahProduk('',2));
    }

    public function testTambahProdukTanpaQuantity()
    {
        $keranjang = new Cart();
        $this->assertEquals(false,$keranjang->tambahProduk('Topi',0));
    }

    public function testTambahProdukTanpaJudulDanQuantity()
    {
        $keranjang = new Cart();
        $this->assertEquals(false,$keranjang->tambahProduk('',0));
    }

    public function testTambahProdukQtyBukanInteger()
    {
        $keranjang = new Cart();
        $this->assertEquals(false,$keranjang->tambahProduk('Topi','quantity'));
    }

    public function testHapusProduk()
    {
        $keranjang = new Cart();
        $keranjang->tambahProduk('Topi',2);
        $this->assertEquals(true,$keranjang->hapusProduk('Topi'));
    }

    public function testHapusProdukSalahKode()
    {
        $keranjang = new Cart();
        $keranjang->tambahProduk('Topi',2);
        $this->assertEquals(false,$keranjang->hapusProduk('Tpi'));
    }
}