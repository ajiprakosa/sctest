<?php
class Cart {
    private $line_items;

    private function helper_search_key_in_cart($kode){
        if(!empty($this->line_items)){
            foreach($this->line_items as $k => $_item){
                if(strtolower($_item['kode_produk'])==strtolower($kode)){
                    return $k;
                    break;
                }
            }
        }else{
            return false;
        }
    }
    
    public function tambahProduk($kodeProduk,$kuantitas){
        if(!empty($kodeProduk) && is_integer($kuantitas) && !empty($kuantitas)){
            $incart_key =$this->helper_search_key_in_cart($kodeProduk);
            if(is_numeric($incart_key)){
                $this->line_items[$incart_key]['kuantitas']+=$kuantitas;
            }else{
                $this->line_items[]=array(
                    'kode_produk'=>$kodeProduk,
                    'kuantitas'=>$kuantitas
                );
            }
            return true;       
        }        
        return false;        
    }

    public function hapusProduk($kodeProduk){
        $incart_key = $this->helper_search_key_in_cart($kodeProduk);
        if(is_numeric($incart_key)){
           unset($this->line_items[$incart_key]);
           return true;
        }
        return false;
    }

    public function tampilkanCart(){
        $items = array();
        if(!empty($this->line_items)){
            foreach($this->line_items as $_item){
                $items[]=$_item['kode_produk'].' ('.$_item['kuantitas'].')';
            }
        }
        print implode('<br>',$items);
    }

}
